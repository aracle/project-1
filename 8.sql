create or replace Function P_F2 (i IN NUMBER)
Return Number
AS c Number;

BEGIN
   Select Count(G_ID) INTO c From G_F Where F_ID = i;
   return c;
END;
