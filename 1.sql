CREATE TABLE Nationality (
  ID NUMBER(10) NOT NULL,
  Name VARCHAR2(255) NULL,  
  PRIMARY KEY (ID))
;

CREATE TABLE People (
  ID NUMBER(10) NOT NULL,
  Name VARCHAR2(255) NULL,
  Surname VARCHAR2(255) NULL,
  Patronymic VARCHAR2(255) NULL,
  B_Day DATE NULL,
  N_ID NUMBER(10) NOT NULL,
  PRIMARY KEY (ID),
  
  CONSTRAINT fk_People_Nationality
    FOREIGN KEY (N_ID)
    REFERENCES Nationality (ID)  
  )
;

CREATE TABLE Awards (
  ID NUMBER(10) NOT NULL,
  Name VARCHAR2(255) NULL,  
  PRIMARY KEY (ID))
;

CREATE TABLE P_A (
  P_ID NUMBER(10) NOT NULL,
  A_ID NUMBER(10) NOT NULL,  
  PRIMARY KEY (P_ID, A_ID),
  
  CONSTRAINT fk_Awards_P_A
    FOREIGN KEY (A_ID)
    REFERENCES Awards (ID),
  CONSTRAINT fk_People_P_A
    FOREIGN KEY (P_ID)
    REFERENCES People (ID)
  )
;

CREATE TABLE Films (
  ID NUMBER(10) NOT NULL,
  Year_R NUMBER(10) NULL,
  Rating NUMBER(10, 3),
  Name VARCHAR2(255) NULL,  
  PRIMARY KEY (ID))
;

CREATE TABLE Reviews (
  ID NUMBER(10) NOT NULL,
  Author VARCHAR2(255) NULL,
  Text VARCHAR2(255) NULL,
  ID_F NUMBER(10) NOT NULL,   
  PRIMARY KEY (ID),
  
  CONSTRAINT fk_Reviews_Films
    FOREIGN KEY (ID_F)
    REFERENCES Films (ID)
  )
;

CREATE TABLE Genre (
  ID NUMBER(10) NOT NULL,
  Name VARCHAR2(255),   
  PRIMARY KEY (ID))
;

CREATE TABLE G_F (
  F_ID NUMBER(10) NOT NULL,
  G_ID NUMBER(10) NOT NULL,
  PRIMARY KEY (F_ID, G_ID),
  
  CONSTRAINT fk_Films_G_F
    FOREIGN KEY (F_ID)
    REFERENCES Films (ID),
  CONSTRAINT fk_Genre_G_F
    FOREIGN KEY (G_ID)
    REFERENCES Genre (ID)
  )
;

CREATE TABLE S_F (
  S_ID NUMBER(10) NOT NULL,
  F_ID NUMBER(10) NOT NULL,
  PRIMARY KEY (S_ID, F_ID),
  
  CONSTRAINT fk_People_S_F
    FOREIGN KEY (S_ID)
    REFERENCES People (ID),
  CONSTRAINT fk_Films_S_F
    FOREIGN KEY (F_ID)
    REFERENCES Films (ID)
  )
;

CREATE TABLE A_F (
  A_ID NUMBER(10) NOT NULL,
  F_ID NUMBER(10) NOT NULL,
  PRIMARY KEY (A_ID, F_ID),
  
  CONSTRAINT fk_People_A_F
    FOREIGN KEY (A_ID)
    REFERENCES People (ID),
  CONSTRAINT fk_Films_A_F
    FOREIGN KEY (F_ID)
    REFERENCES Films (ID)
  )
;

CREATE TABLE R_F (
  R_ID NUMBER(10) NOT NULL,
  F_ID NUMBER(10) NOT NULL,
  PRIMARY KEY (R_ID, F_ID),
  
  CONSTRAINT fk_People_R_F
    FOREIGN KEY (R_ID)
    REFERENCES People (ID),
  CONSTRAINT fk_Films_R_F
    FOREIGN KEY (F_ID)
    REFERENCES Films (ID)
  )
;

Insert into Genre (ID, Name) values (1, 'Триллер');
Insert into Genre (ID, Name) values (2, 'Ужастик');
Insert into Genre (ID, Name) values (3, 'Комедия');

Insert into Films (ID, Year_R, Rating, Name) values (1, 1995, 8.9, 'Фильм 1');
Insert into Films (ID, Year_R, Rating, Name) values (2, 1987, 7.1, 'Фильм 2');
Insert into Films (ID, Year_R, Rating, Name) values (3, 2001, 8.3, 'Фильм 3');
Insert into Films (ID, Year_R, Rating, Name) values (4, 2009, 5.1, 'Фильм 4');
Insert into Films (ID, Year_R, Rating, Name) values (5, 1999, 4.9, 'Фильм 5');
Insert into Films (ID, Year_R, Rating, Name) values (6, 1994, 5.9, 'Фильм 6');

Insert into Reviews (ID, Author, Text, ID_F) values (1, 'Моисей', 'Рецензия 1', 1);
Insert into Reviews (ID, Author, Text, ID_F) values (2, 'Моисей', 'Рецензия 2', 2);
Insert into Reviews (ID, Author, Text, ID_F) values (3, 'Моисей', 'Рецензия 3', 3);
Insert into Reviews (ID, Author, Text, ID_F) values (4, 'Моисей', 'Рецензия 4', 4);
Insert into Reviews (ID, Author, Text, ID_F) values (5, 'Моисей', 'Рецензия 5', 5);
Insert into Reviews (ID, Author, Text, ID_F) values (6, 'Моисей', 'Рецензия 6', 6);

Insert into G_F (F_ID, G_ID) values (1, 1);
Insert into G_F (F_ID, G_ID) values (1, 2);
Insert into G_F (F_ID, G_ID) values (2, 3);
Insert into G_F (F_ID, G_ID) values (3, 3);
Insert into G_F (F_ID, G_ID) values (3, 1);
Insert into G_F (F_ID, G_ID) values (4, 1);
Insert into G_F (F_ID, G_ID) values (5, 1);
Insert into G_F (F_ID, G_ID) values (6, 1);
Insert into G_F (F_ID, G_ID) values (5, 2);
Insert into G_F (F_ID, G_ID) values (6, 3);

Insert into Awards (ID, Name) values (1, 'Награда 1');
Insert into Awards (ID, Name) values (2, 'Награда 2');
Insert into Awards (ID, Name) values (3, 'Награда 3');
Insert into Awards (ID, Name) values (4, 'Награда 4');
Insert into Awards (ID, Name) values (5, 'Награда 5');
Insert into Awards (ID, Name) values (6, 'Награда 6');

Insert into Nationality (ID, Name) values (1, 'Национальность 1');
Insert into Nationality (ID, Name) values (2, 'Национальность 2');
Insert into Nationality (ID, Name) values (3, 'Национальность 3');
Insert into Nationality (ID, Name) values (4, 'Национальность 4');
Insert into Nationality (ID, Name) values (5, 'Национальность 5');

Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (1, 'Мышкин', 'Харитон', 'Созонович', '03.29.1983', 1);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (2, 'Елисеев', 'Азарий', 'Владимирович', '12.28.1988', 2);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (3, 'Родионов', 'Ярослав', 'Геннадиевич', '06.21.1974', 3);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (4, 'Громов', 'Анатолий', 'Семёнович', '12.03.1973', 4);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (5, 'Волков', 'Дмитрий', 'Федосеевич', '03.08.1982', 5);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (6, 'Баранова', 'Каролина', 'Якововна', '05.05.1981', 1);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (7, 'Прохорова', 'Сильва', 'Германновна', '08.05.1985', 2);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (8, 'Нестерова', 'Симона', 'Васильевна', '09.15.1980', 3);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (9, 'Воронова', 'Амина', 'Робертовна', '08.08.1980', 4);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (10, 'Волкова', 'Ася', 'Фроловна', '03.28.1986', 5);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (11, 'Гущина', 'Арина', 'Георгьевна', '12.19.1986', 1);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (12, 'Лыткина', 'Таисия', 'Рудольфовна', '07.03.1986', 2);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (13, 'Вишнякова', 'Лия', 'Максовна', '06.04.1984', 3);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (14, 'Кулакова', 'Елизавета', 'Данииловна', '12.12.1986', 4);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (15, 'Гришина', 'Мишель', 'Пётровна', '06.16.1982', 5);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (16, 'Родионова', 'Кармелитта', 'Вадимовна', '07.14.1988', 1);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (17, 'Лыткина', 'Санда', 'Мэлсовна', '04.01.1987', 2);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (18, 'Соболева', 'Агнесса', 'Владленовна', '08.13.1985', 3);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (19, 'Нестерова', 'Алиса', 'Григорьевна', '08.18.1980', 4);
Insert into People (ID, Name, Surname, Patronymic, B_Day, N_ID) values (20, 'Аксёнова', 'Сафина', 'Святославовна', '04.27.1988', 5);

Insert into P_A (P_ID, A_ID) values (1, 1);
Insert into P_A (P_ID, A_ID) values (2, 2);
Insert into P_A (P_ID, A_ID) values (3, 3);
Insert into P_A (P_ID, A_ID) values (1, 4);
Insert into P_A (P_ID, A_ID) values (1, 5);

Insert into S_F (S_ID, F_ID) values (1, 1);
Insert into S_F (S_ID, F_ID) values (2, 2);
Insert into S_F (S_ID, F_ID) values (3, 3);
Insert into S_F (S_ID, F_ID) values (3, 4);
Insert into S_F (S_ID, F_ID) values (4, 4);
Insert into S_F (S_ID, F_ID) values (4, 5);
Insert into S_F (S_ID, F_ID) values (4, 6);
Insert into S_F (S_ID, F_ID) values (5, 6);
Insert into S_F (S_ID, F_ID) values (6, 6);

Insert into R_F (R_ID, F_ID) values (1, 1);
Insert into R_F (R_ID, F_ID) values (20, 2);
Insert into R_F (R_ID, F_ID) values (20, 3);
Insert into R_F (R_ID, F_ID) values (20, 4);
Insert into R_F (R_ID, F_ID) values (20, 5);
Insert into R_F (R_ID, F_ID) values (20, 6);
Insert into R_F (R_ID, F_ID) values (19, 6);

Insert into A_F (A_ID, F_ID) values (1, 1);
Insert into A_F (A_ID, F_ID) values (2, 2);
Insert into A_F (A_ID, F_ID) values (3, 3);
Insert into A_F (A_ID, F_ID) values (4, 4);
Insert into A_F (A_ID, F_ID) values (5, 5);
Insert into A_F (A_ID, F_ID) values (6, 6);
Insert into A_F (A_ID, F_ID) values (7, 1);
Insert into A_F (A_ID, F_ID) values (8, 2);
Insert into A_F (A_ID, F_ID) values (9, 2);
Insert into A_F (A_ID, F_ID) values (10, 3);
Insert into A_F (A_ID, F_ID) values (11, 4);
Insert into A_F (A_ID, F_ID) values (12, 5);
Insert into A_F (A_ID, F_ID) values (13, 4);
Insert into A_F (A_ID, F_ID) values (14, 4);
Insert into A_F (A_ID, F_ID) values (15, 5);
Insert into A_F (A_ID, F_ID) values (16, 6);
Insert into A_F (A_ID, F_ID) values (17, 4);
Insert into A_F (A_ID, F_ID) values (18, 3);
Insert into A_F (A_ID, F_ID) values (19, 2);
Insert into A_F (A_ID, F_ID) values (10, 1);