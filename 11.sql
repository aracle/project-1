create or replace TRIGGER P_T2
before INSERT ON People
FOR EACH ROW
Declare
v_date date;
BEGIN
select to_date(:new.b_day,'mon.dd.yyyy') into v_date from dual;
exception when others then raise_application_error (-20001,'Не верный формат!!!');
END;
