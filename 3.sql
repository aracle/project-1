create or replace PROCEDURE P_Pr2 (i IN NUMBER)
AS Cursor get_screenwriter(i NUMBER) is 
	Select p.ID, p.Surname, p.Name, p.Patronymic, p.B_Day, n.Name as Nat From
        (Select * From 
        (Select S_F.S_ID, Count(f.ID) as colvo
        From S_F, Films f, G_F
        Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
        Group by S_F.S_ID) t1
        Where t1.colvo = (Select MAX(t2.Colvo) from (Select S_F.S_ID, Count(f.ID) as colvo
        From S_F, Films f, G_F
        Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
        Group by S_F.S_ID) t2)) t3, People p, Nationality n
        Where p.ID = t3.S_ID and p.N_ID = n.ID;
BEGIN
	for h IN get_screenwriter(i)
	LOOP
		dbms_output.put_line('ID: '||h.ID||'; '||h.Surname||' '||h.Name||' '||h.Patronymic||'; ДР: '||h.B_Day||'; Национальность: '||h.Nat);
	END LOOP;
END;
