create or replace PROCEDURE P_Pr3 (i IN NUMBER)
AS Cursor get_screenwriter(i NUMBER) is 
	Select S_F.S_ID as Screenwriter, Count(f.id) as colvo
	From S_F, Films f
	Where f.Year_R = i and f.id = S_F.F_ID
	Group by S_F.S_ID;
BEGIN
	for h IN get_screenwriter(i)
	LOOP
		dbms_output.put_line('ID: '||h.Screenwriter||'; Count Films: '||h.colvo);
	END LOOP;
END;