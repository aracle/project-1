create or replace TRIGGER P_T1
instead of INSERT ON P_V1
FOR EACH ROW
Declare
x_p Number;
x_n Number;
x_f Number;
BEGIN
Select Max(id) INTO x_p from people;
Select Max(id) INTO x_n from Nationality;
Select Max(id) INTO x_f from Films;
INSERT INTO Nationality (id, Name) VALUES (x_n + 1, :new.Nat_Name);
INSERT INTO People (id, Name, Surname, n_id) VALUES (x_p + 1, :new.Name, :new.Surname, x_n + 1);
INSERT INTO Films (id, Name, Rating) VALUES (x_f + 1, :new.F_Name, :new.Rating);
INSERT INTO R_F (R_ID, F_ID) VALUES (x_p + 1, x_f + 1);
END;
