PACKAGE


create or replace package MP1_P1 is
F1 number;
F2 number;

function P_F1 (i IN Number) RETURN Number;
function P_F2 (i IN Number) RETURN Number;

procedure P_Pr1 (i IN NUMBER, j IN NUMBER);
procedure P_Pr2 (i IN NUMBER);
procedure P_Pr3 (i in Number);
procedure P_Pr4;
procedure P_Pr5 (i IN NUMBER);

END MP1_P1;


BODY


create or replace package body MP1_P1 is

PROCEDURE P_Pr1 (i IN NUMBER, j IN NUMBER)
AS Cursor get_actors(i NUMBER, j NUMBER) is 
	Select * From
        (Select p.id, Count(A_F.A_ID) as colvo
        From People p, A_F, Films f
        Where p.id = A_F.A_ID and f.ID = A_F.F_ID and f.Rating > j
        Group by p.ID) t1
        Where t1.colvo = i;
BEGIN
	for h IN get_actors(i, j)
	LOOP
		dbms_output.put_line('ID: '||h.ID);
	END LOOP;
END;

PROCEDURE P_Pr2 (i IN NUMBER)
AS Cursor get_screenwriter(i NUMBER) is 
	Select p.ID, p.Surname, p.Name, p.Patronymic, p.B_Day, n.Name as Nat From
        (Select * From 
        (Select S_F.S_ID, Count(f.ID) as colvo
        From S_F, Films f, G_F
        Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
        Group by S_F.S_ID) t1
        Where t1.colvo = (Select MAX(t2.Colvo) from (Select S_F.S_ID, Count(f.ID) as colvo
        From S_F, Films f, G_F
        Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
        Group by S_F.S_ID) t2)) t3, People p, Nationality n
        Where p.ID = t3.S_ID and p.N_ID = n.ID;
BEGIN
	for h IN get_screenwriter(i)
	LOOP
		dbms_output.put_line('ID: '||h.ID||'; '||h.Surname||' '||h.Name||' '||h.Patronymic||'; ДР: '||h.B_Day||'; Национальность: '||h.Nat);
	END LOOP;
END;

PROCEDURE P_Pr3 (i IN NUMBER)
AS Cursor get_screenwriter(i NUMBER) is 
	Select S_F.S_ID as Screenwriter, Count(f.id) as colvo
	From S_F, Films f
	Where f.Year_R = i and f.id = S_F.F_ID
	Group by S_F.S_ID;
BEGIN
	for h IN get_screenwriter(i)
	LOOP
		dbms_output.put_line('ID: '||h.Screenwriter||'; Count Films: '||h.colvo);
	END LOOP;
END;

PROCEDURE P_Pr4
AS Cursor get_reg is 	
      Select R_F.R_ID as id from A_F, R_F
      Where A_F.A_ID = R_F.R_ID;
      x Number := 0;
BEGIN
	for h IN get_reg()
	LOOP
        x := x + 1;
		dbms_output.put_line('ID: '||h.ID);
	END LOOP;   
    if x = 0 then raise_application_error (-20001,'Таких нет');
    end if;        
END;

PROCEDURE P_Pr5 (i IN NUMBER)
AS Cursor get_actor(i NUMBER) is 
	Select p.Name, p.Surname, p.B_Day From People p,
	(Select A_F.A_ID from A_F, Films, G_F
	Where G_F.F_ID = Films.ID and Films.ID = A_F.F_ID and G_F.G_ID = i) t1
	Where p.ID = t1.A_ID;
    x Number := 0;
BEGIN
    for h IN get_actor(i)       
	LOOP
    x := x + 1;
		dbms_output.put_line(h.Name||' '||h.Surname||' '||h.B_Day);
	END LOOP;  
    if x = 0 then raise_application_error (-20001,'Таких нет');
    end if;        
END;

Function P_F1 (i IN NUMBER)
Return Number
AS c Number;
x Number;
BEGIN
    Select Count(*) into x from (Select s_id From 
    (Select S_F.S_ID, Count(f.ID) as colvo
    From S_F, Films f, G_F
    Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
    Group by S_F.S_ID) t1
    Where t1.colvo = (Select MAX(t2.Colvo) from (Select S_F.S_ID, Count(f.ID) as colvo
    From S_F, Films f, G_F
    Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
    Group by S_F.S_ID) t2)) ;
    if x > 1 then raise_application_error (-20001,'Их больше чем 1');
    else
    Select s_id INTO c From 
    (Select S_F.S_ID, Count(f.ID) as colvo
    From S_F, Films f, G_F
    Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
    Group by S_F.S_ID) t1
    Where t1.colvo = (Select MAX(t2.Colvo) from (Select S_F.S_ID, Count(f.ID) as colvo
    From S_F, Films f, G_F
    Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
    Group by S_F.S_ID) t2);    
        return c;
    end if;
END;

Function P_F2 (i IN NUMBER)
Return Number
AS c Number;
BEGIN
   Select Count(G_ID) INTO c From G_F Where F_ID = i;
   return c;
END;

BEGIN
  F1 := P_F1(1);
  F2 := P_F2(1);
END;