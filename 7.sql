create or replace Function P_F1 (i IN NUMBER)
Return Number
AS c Number;
x Number;

BEGIN
    Select Count(*) into x from (Select s_id From 
    (Select S_F.S_ID, Count(f.ID) as colvo
    From S_F, Films f, G_F
    Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
    Group by S_F.S_ID) t1
    Where t1.colvo = (Select MAX(t2.Colvo) from (Select S_F.S_ID, Count(f.ID) as colvo
    From S_F, Films f, G_F
    Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
    Group by S_F.S_ID) t2)) ;
    if x > 1 then raise_application_error (-20001,'Их больше чем 1');
    else
    Select s_id INTO c From 
    (Select S_F.S_ID, Count(f.ID) as colvo
    From S_F, Films f, G_F
    Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
    Group by S_F.S_ID) t1
    Where t1.colvo = (Select MAX(t2.Colvo) from (Select S_F.S_ID, Count(f.ID) as colvo
    From S_F, Films f, G_F
    Where S_F.F_ID = F.ID and F.ID = G_F.F_ID and G_F.G_ID = i
    Group by S_F.S_ID) t2);    
        return c;
    end if;
END;