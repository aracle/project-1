Create or replace view P_V1 as
Select p.Name, p.Surname, n.Name as nat_name, f.Name as f_name, f.Rating
From People p, R_F, Films f, Nationality n
Where n.id = p.N_ID and p.ID = R_F.R_ID and R_F.F_ID = f.ID and f.Rating > 7 and n.Name = 'Национальность 5'