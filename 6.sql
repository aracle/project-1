create or replace PROCEDURE P_Pr5 (i IN NUMBER)
AS Cursor get_actor(i NUMBER) is 
	Select p.Name, p.Surname, p.B_Day From People p,
	(Select A_F.A_ID from A_F, Films, G_F
	Where G_F.F_ID = Films.ID and Films.ID = A_F.F_ID and G_F.G_ID = i) t1
	Where p.ID = t1.A_ID;
    x Number := 0;
BEGIN
    for h IN get_actor(i)       
	LOOP
    x := x + 1;
		dbms_output.put_line(h.Name||' '||h.Surname||' '||h.B_Day);
	END LOOP;  
    if x = 0 then raise_application_error (-20001,'Таких нет');
    end if;        
END;