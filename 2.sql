create or replace PROCEDURE P_Pr1 (i IN NUMBER, j IN NUMBER)
AS Cursor get_actors(i NUMBER, j NUMBER) is 
	Select * From
        (Select p.id, Count(A_F.A_ID) as colvo
        From People p, A_F, Films f
        Where p.id = A_F.A_ID and f.ID = A_F.F_ID and f.Rating > j
        Group by p.ID) t1
        Where t1.colvo = i;
BEGIN
	for h IN get_actors(i, j)
	LOOP
		dbms_output.put_line('ID: '||h.ID);
	END LOOP;
END;
